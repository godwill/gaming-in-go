package main

import (
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
)

const basicEnemySize = 105

func newBasicEnemy(renderer *sdl.Renderer, position vector) *element {
	basicEnemy := &element{}
	basicEnemy.position = position
	basicEnemy.rotation = 180

	idleSequence, err := newSequence(renderer, "sprites/basic_enemy/idle", 5, true)
	if err != nil {
		panic(fmt.Errorf("creating idle sequence"))
	}

	destroySequence, err := newSequence(renderer, "sprites/basic_enemy/destroy", 15, false)
	if err != nil {
		panic(fmt.Errorf("creating destroy sequence"))
	}

	sequences := map[string]*sequence{
		"idle":    idleSequence,
		"destroy": destroySequence,
	}

	animator := newAnimator(basicEnemy, sequences, "idle")
	basicEnemy.addComponent(animator)

	vuln := newVulnerableToBullets(basicEnemy)
	basicEnemy.addComponent(vuln)

	collision := circle{
		center: basicEnemy.position,
		radius: 38,
	}
	basicEnemy.collisions = append(basicEnemy.collisions, collision)

	basicEnemy.active = true

	return basicEnemy
}
