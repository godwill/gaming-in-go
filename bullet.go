package main

import (
	"github.com/veandco/go-sdl2/sdl"
)

const bulletSpeed = 20

func newBullet(renderer *sdl.Renderer) *element {
	bullet := &element{}

	sprite := newSpriteRenderer(bullet, renderer, "sprites/player_bullet.bmp")
	bullet.addComponent(sprite)

	mover := newBulletMover(bullet, bulletSpeed)
	bullet.addComponent(mover)

	collision := circle{
		center: bullet.position,
		radius: 8,
	}
	bullet.collisions = append(bullet.collisions, collision)

	bullet.tag = "bullet"
	return bullet
}

var bulletPool []*element

func initBulletPull(renderer *sdl.Renderer) {
	for i := 0; i < 30; i++ {
		newBullet := newBullet(renderer)
		elements = append(elements, newBullet)
		bulletPool = append(bulletPool, newBullet)
	}
}

func bulletFromPool() (*element, bool) {
	for _, bullet := range bulletPool {
		if !bullet.active {
			return bullet, true
		}
	}

	return nil, false
}
