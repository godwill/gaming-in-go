package main

import "github.com/veandco/go-sdl2/sdl"

type keyboardMover struct {
	container *element
	speed     float64

	spriteRenderer *spriteRenderer
}

func newKeyboardMover(container *element, speed float64) *keyboardMover {
	return &keyboardMover{
		container:      container,
		speed:          speed,
		spriteRenderer: container.getComponent(&spriteRenderer{}).(*spriteRenderer),
	}
}

func (m *keyboardMover) onUpdate() error {
	keys := sdl.GetKeyboardState()

	if keys[sdl.SCANCODE_LEFT] == 1 {
		if m.container.position.x-(playerSize/2.0) > 0 {
			m.container.position.x -= m.speed * delta
		}
	} else if keys[sdl.SCANCODE_RIGHT] == 1 {
		if m.container.position.x+(playerSize/2.0) < screenWidth {
			m.container.position.x += m.speed * delta
		}
	}

	return nil
}

func (m *keyboardMover) onDraw(renderer *sdl.Renderer) error {
	return nil
}

func (m *keyboardMover) onCollision(other *element) error {
	return nil
}
