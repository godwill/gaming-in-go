package main

import (
	"github.com/veandco/go-sdl2/sdl"
	"math"
	"time"
)

type shooter struct {
	container *element
	coolDown  time.Duration
	lastShot  time.Time
}

func newShooter(container *element, coolDown time.Duration) *shooter {
	return &shooter{
		container: container,
		coolDown:  coolDown,
	}
}

func (s *shooter) onUpdate() error {
	keys := sdl.GetKeyboardState()

	if keys[sdl.SCANCODE_SPACE] == 1 {
		if time.Since(s.lastShot) >= playerShotCoolDown {
			s.shoot(s.container.position.x+25, s.container.position.y-20)
			s.shoot(s.container.position.x-25, s.container.position.y-20)
			s.lastShot = time.Now()
		}
	}

	return nil
}

func (s *shooter) shoot(xOffset, yOffset float64) {
	if bul, ok := bulletFromPool(); ok {
		bul.active = true
		bul.position.x = xOffset
		bul.position.y = yOffset
		bul.rotation = 270 * (math.Pi / 180)
	}
}

func (s *shooter) onDraw(renderer *sdl.Renderer) error {
	return nil
}

func (s *shooter) onCollision(other *element) error {
	return nil
}
