package main

import (
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
)

func drawTexture(renderer *sdl.Renderer, texture *sdl.Texture, position vector, rotation float64) error {
	_, _, width, height, err := texture.Query()
	if err != nil {
		panic(fmt.Errorf("querrying texture: %v", err))
	}

	x := position.x - float64(width/2.0)
	y := position.y - float64(height/2.0)

	return renderer.CopyEx(texture,
		&sdl.Rect{X: 0, Y: 0, W: width, H: height},
		&sdl.Rect{X: int32(x), Y: int32(y), W: width, H: height},
		rotation,
		&sdl.Point{X: width / 2, Y: height / 2},
		sdl.FLIP_NONE,
	)

}
