package main

import (
	"github.com/veandco/go-sdl2/sdl"
	"time"
)

type animator struct {
	container       *element
	sequences       map[string]*sequence
	current         string
	lastFrameChange time.Time
	finished        bool
}

func newAnimator(container *element, sequences map[string]*sequence, defaultSequence string) *animator {
	var animator animator
	animator.container = container
	animator.sequences = sequences
	animator.current = defaultSequence
	animator.lastFrameChange = time.Now()

	return &animator
}

func (a *animator) onUpdate() error {
	sequence := a.sequences[a.current]
	frameInterval := float64(time.Second) / sequence.sampleRate

	if time.Since(a.lastFrameChange) >= time.Duration(frameInterval) {
		a.finished = sequence.nextFrame()
		a.lastFrameChange = time.Now()
	}
	return nil
}

func (a *animator) onDraw(renderer *sdl.Renderer) error {
	texture := a.sequences[a.current].texture()
	return drawTexture(
		renderer, texture, a.container.position, a.container.rotation)
}

func (a *animator) onCollision(other *element) error {
	return nil
}

func (a *animator) setSequence(name string) {
	a.current = name
	a.lastFrameChange = time.Now()
}
