package main

import (
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
	"io/ioutil"
	"path"
)

type sequence struct {
	textures   []*sdl.Texture
	frame      int
	sampleRate float64
	loop       bool
}

func newSequence(renderer *sdl.Renderer, filepath string, sampleRate float64, loop bool) (*sequence, error) {
	files, err := ioutil.ReadDir(filepath)
	if err != nil {
		return nil, fmt.Errorf("reading directory %v: %v", filepath, err)
	}

	var textures []*sdl.Texture
	for _, file := range files {
		fileName := path.Join(filepath, file.Name())
		t, err := textureFromBMP(renderer, fileName)
		if err != nil {
			return nil, err
		}

		textures = append(textures, t)
	}

	return &sequence{textures: textures, sampleRate: sampleRate, loop: loop}, nil
}

func (s *sequence) texture() *sdl.Texture {
	return s.textures[s.frame]
}

func (s *sequence) nextFrame() bool {
	if s.frame == len(s.textures)-1 {
		if s.loop {
			s.frame = 0
		} else {
			return true
		}
	} else {
		s.frame++
	}

	return false
}
