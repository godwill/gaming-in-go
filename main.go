package main

import (
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
	"time"
)

const (
	screenWidth  = 600
	screenHeight = 800

	targetTickerTime = 60
)

var delta float64

func main() {
	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		panic(err)
	}
	defer sdl.Quit()

	window, err := sdl.CreateWindow("Gaming in Go", sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		screenWidth, screenHeight, sdl.WINDOW_SHOWN)
	if err != nil {
		panic(err)
	}
	defer window.Destroy()

	renderer, err := sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		fmt.Println("initializing window: ", err)
	}

	defer renderer.Destroy()

	player := newPlayer(renderer)
	if err != nil {
		fmt.Println("creating player: ", err)
	}
	elements = append(elements, player)

	for i := 0; i < 5; i++ {
		for j := 0; j < 3; j++ {
			x := (float64(i)/5)*screenWidth + (basicEnemySize / 2.0)
			y := float64(j)*basicEnemySize + (basicEnemySize / 2.0)
			pos := vector{
				x: x,
				y: y,
			}
			enemy := newBasicEnemy(renderer, pos)
			elements = append(elements, enemy)
		}
	}

	initBulletPull(renderer)

	for {
		frameStartTime := time.Now()

		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch event.(type) {
			case *sdl.QuitEvent:
				return
			}
		}

		renderer.SetDrawColor(255, 255, 255, 255)
		renderer.Clear()

		for _, element := range elements {
			if element.active {
				err = element.update()
				if err != nil {
					fmt.Println("updating element: ", err)
				}

				err = element.draw(renderer)
				if err != nil {
					fmt.Println("drawing element: ", err)
				}
			}
		}

		err := checkCollisions()
		if err != nil {
			fmt.Println("checking collisions: ", err)
			return
		}

		renderer.Present()

		delta = time.Since(frameStartTime).Seconds() * targetTickerTime
	}
}
