package main

import (
	"github.com/veandco/go-sdl2/sdl"
)

type vector struct {
	x, y float64
}

type element struct {
	position   vector
	rotation   float64
	active     bool
	tag        string
	collisions []circle
	components []component
}

func (e *element) draw(renderer *sdl.Renderer) error {
	for _, comp := range e.components {
		err := comp.onDraw(renderer)
		if err != nil {
			return err
		}
	}

	return nil
}

func (e *element) update() error {
	for _, comp := range e.components {
		err := comp.onUpdate()
		if err != nil {
			return err
		}
	}

	return nil
}

func (e *element) collision(other *element) error {
	for _, comp := range e.components {
		err := comp.onCollision(other)
		if err != nil {
			return err
		}
	}

	return nil
}

var elements []*element
