package main

import (
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
)

type spriteRenderer struct {
	container     *element
	texture       *sdl.Texture
	width, height float64
}

func newSpriteRenderer(container *element, renderer *sdl.Renderer, filename string) *spriteRenderer {
	texture, err := textureFromBMP(renderer, filename)
	if err != nil {
		panic(err)
	}

	_, _, width, height, err := texture.Query()
	if err != nil {
		panic(fmt.Errorf("querrying texture: %v", err))
	}

	return &spriteRenderer{
		container: container,
		texture:   texture,
		width:     float64(width),
		height:    float64(height),
	}
}

func (s *spriteRenderer) onDraw(renderer *sdl.Renderer) error {
	return drawTexture(renderer, s.texture, s.container.position, s.container.rotation)
}

func (s *spriteRenderer) onUpdate() error {
	return nil
}

func (s *spriteRenderer) onCollision(other *element) error {
	return nil
}

func textureFromBMP(renderer *sdl.Renderer, filename string) (*sdl.Texture, error) {
	img, err := sdl.LoadBMP(filename)
	if err != nil {
		return nil, fmt.Errorf("loading %v: %v", filename, err)
	}

	defer img.Free()

	tex, err := renderer.CreateTextureFromSurface(img)
	if err != nil {
		return nil, fmt.Errorf("creating texture from %v: %v", filename, err)
	}

	return tex, nil
}
