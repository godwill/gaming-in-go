package main

import (
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
	"reflect"
)

type component interface {
	onUpdate() error
	onDraw(renderer *sdl.Renderer) error
	onCollision(other *element) error
}

func (e *element) addComponent(comp component) {
	for _, c := range e.components {
		if reflect.TypeOf(comp) == reflect.TypeOf(c) {
			panic(fmt.Errorf("attempt to add component with existing type %v", reflect.TypeOf(comp)))
		}
	}

	e.components = append(e.components, comp)

}

func (e *element) getComponent(withType component) component {
	typ := reflect.TypeOf(withType)
	for _, comp := range e.components {
		if reflect.TypeOf(comp) == typ {
			return comp
		}
	}

	panic(fmt.Sprintf("no component with type %v", reflect.TypeOf(withType)))
}
