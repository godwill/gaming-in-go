package main

import (
	"github.com/veandco/go-sdl2/sdl"
)

type vulnerableToBullets struct {
	container *element
	animator  *animator
}

func newVulnerableToBullets(container *element) *vulnerableToBullets {
	return &vulnerableToBullets{container: container, animator: container.getComponent(&animator{}).(*animator)}
}

func (v *vulnerableToBullets) onUpdate() error {
	if v.animator.finished && v.animator.current == "destroy"{
		v.container.active = false
		return nil
	}
	return nil
}

func (v *vulnerableToBullets) onDraw(renderer *sdl.Renderer) error {
	return nil
}

func (v *vulnerableToBullets) onCollision(other *element) error {
	if other.tag == "bullet" {
		v.animator.setSequence("destroy")
	}

	return nil
}
