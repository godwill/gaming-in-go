package main

import (
	"github.com/veandco/go-sdl2/sdl"
	"math"
)

type bulletMover struct {
	container *element
	speed     float64
}

func newBulletMover(container *element, speed float64) *bulletMover {
	return &bulletMover{
		container: container,
		speed:     speed,
	}
}

func (b *bulletMover) onDraw(renderer *sdl.Renderer) error {
	return nil
}

func (b *bulletMover) onUpdate() error {
	container := b.container

	container.position.x += bulletSpeed * math.Cos(container.rotation) * delta
	container.position.y += bulletSpeed * math.Sin(container.rotation) * delta

	if container.position.x > screenWidth || container.position.x < 0 || container.position.y > screenHeight || container.position.y < 0 {
		b.container.active = false
	}

	container.collisions[0].center = container.position
	return nil
}

func (b *bulletMover) onCollision(other *element) error {
	b.container.active = false
	return nil
}
