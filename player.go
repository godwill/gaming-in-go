package main

import (
	"github.com/veandco/go-sdl2/sdl"
	"time"
)

const (
	playerSpeed        = 5
	playerSize         = 105
	playerShotCoolDown = time.Millisecond * 250
)

func newPlayer(renderer *sdl.Renderer) *element {
	player := &element{}

	player.position = vector{
		x: screenWidth / 2.0,
		y: screenHeight - playerSize/2.0,
	}

	sprite := newSpriteRenderer(player, renderer, "sprites/player.bmp")
	player.addComponent(sprite)

	mover := newKeyboardMover(player, playerSpeed)
	player.addComponent(mover)

	shooter := newShooter(player, playerShotCoolDown)
	player.addComponent(shooter)

	player.active = true

	return player
}
